﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDrag : MonoBehaviour {

    [SerializeField] private Vector2 defautPos;
    private Vector3 currentPos;
    private Vector3 offset;

    private Shoot ShootScript;

	// Use this for initialization
	void Start () {

        ShootScript = gameObject.GetComponent<Shoot>();

        // La position de départ du viseur
        defautPos = new Vector2 (0.0f, 0.0f);

	}

    private void OnMouseDown()
    {
        // L'offset sert à calculer la distance qui sépare la souris du centre de la sphère, afin de maintenir cet écart
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));

        // On cache le curseur
        Cursor.visible = false;  
    }

    private void OnMouseDrag()
    {
        // On calcule la position en temps réel du viseur
        var currentScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y);
        currentPos = Camera.main.ScreenToWorldPoint(currentScreenPoint) + offset;

        //On déplace le viseur
        transform.position = currentPos;
    }

    private void OnMouseUp()
    {
        //On affiche le curseur
        Cursor.visible = true;

        ShootScript.Shooting();

        // On re téléporte le viseur à la position de départ
        transform.position = defautPos;
    }
}
